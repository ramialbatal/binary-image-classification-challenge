# Binary Image Classification Challenge
This is a classic recruitment chanllenge. Here is the question:

How would you build a classifier with the provided labelled dataset of cow and chicken images?

The images for each class are stored in distinct directories where the directory names give the class labels. The images have been downloaded from ImageNet database by combining synsets relating to each class. In total, there are 4032 images saved as JPEG. The have not been modified from their original dimensions.

The data is zipped to 550 MB and can be downloaded from: XXXX

This is not a competition to build the most accurate classifier possible. The goal in this exercise is to demonstrate competency in handling and manipulating image data, machine learning concepts, and communicating your decision.

Please present your work in a Jupyter Notebook.